import {chromeApi, compose, IChromeTab} from 'chrome-extensions-api';

export interface IJiraField {
    id: string;
    name: string;
    clauseNames: string[];
}

export interface IJiraStatus {
    iconUrl: string;
    id: string;
    name: string;
    statusCategory: {
        color: string;
        id: number;
        name: string;
        key: 'indeterminate' | 'done' | 'new'
    };
}

export interface IHistoryEntry {
    id: string;
    created: string;
    author: {
        key: string;
    };
    items: {
        field: string;
        from: any;
        to: any;
        fromString: string;
        toString: string;
    }[];
}



type TJiraField = string | {} |undefined | IJiraStatus;

export interface IJiraIssue {
    id: string;
    key: string;
    fields: {
        summary?: string;
        status?: IJiraStatus;
        [key: string]: TJiraField;
    };
    self: string;
    changelog?: {
        histories: IHistoryEntry[];
    };
}

export interface ITimeInStatus {
    millis: number;
    status: string;
    duration: string;
}

export interface ISprint {
    activatedDate: string;
    completeDate: string;
    endDate: string;
    goal: string;
    id: number;
    name: string;
    originBoardId: number;
    self: string;
    startDate: string;
    state: 'closed' | 'active' | 'future'
}

export interface IBurndownChangeItem {
    key: string;
    added?: boolean;
    statC?: {
        newValue?: number;
    };
    column?: {
        newStatus: string;
        notDone: boolean;
    };
}

export interface IBurndownChange {
    [timestamp: number]: [IBurndownChangeItem]
}

export interface ISprintBurndown {
    activatedTime: number;
    changes: IBurndownChange;
    completeTime: number;
    endTime: number;
    issueToParentKeys: {};
    issueToSummary: {
        [key: string]: string;
    };
    lastUserWhoClosedHtml: string;
    now: number;
    openCloseChanges: {
        [timestamp: number]: {
            operation: 'OPEN' | 'CLOSE'
        }[]
    };
    startTime: number;
    statisticField: {
        id: string;
        isEnabled: true
        isValid: true
        name: string;
        renderer: string;
        typeId: string;
    };
    workRateData: {
        rates: {
            end: number;
            rate: 1 | 0;
            start: number;
        }[],
        timezone: string;
    };
}

async function GET(url: string): Promise<string> {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = (() => {
            if (xhr.status === 200 && xhr.readyState === 4) {
                resolve(xhr.responseText);
            }
        });
        xhr.send();
    })
}

async function searchJiraIssues(resolve: Function,
                                baseUrl: string,
                                jql: string, {max, chunkSize, fields, history}: {
        max: number,
        chunkSize: number,
        fields: string[],
        history: boolean
    }) {
    let startAt = 0;
    let maxResults = chunkSize;
    let total = 0;
    let done = false;
    let issues: IJiraIssue[] = [];

    let spawnRequest = () => GET(`${baseUrl}/rest/api/2/search?jql=${jql}${history ? '&expand=changelog' : ''}&fields=${fields.join(',')}&startAt=${startAt}&maxResults=${maxResults}`);
    let iterator = {
        [Symbol.asyncIterator]() {
            return {
                async next() {
                    return {
                        value: spawnRequest(),
                        done
                    }
                }
            }
        }
    };

    for await (let stream of iterator) {
        let chunk = JSON.parse(await stream);
        total = chunk.total;
        if (chunk.startAt + maxResults >= total) {
            done = true;
        }
        startAt += maxResults;
        issues = [...issues, ...chunk.issues];

        if (startAt > max) {
            throw new Error('max issue count exceeded');
        }

    }

    let augmentedIssues = null;

    if (history) {
        const historyEntries = issues.map(issue => {
            return (issue.changelog || {histories: []}).histories.map(history => {
                return history.items.map(item => {
                    return {
                        issueId: issue.id,
                        key: issue.key,
                        summary: issue.fields.summary,
                        changeId: history.id,
                        author: history.author.key,
                        created: history.created,
                        timestamp: new Date(history.created).getTime(),
                        field: item.field,
                        from: item.from,
                        to: item.to
                    }
                }).flat()
            }).flat()
        }).flat();


        augmentedIssues = issues.map(item => {
            return {
                ...item,
                history: historyEntries.filter(entry => entry.key === item.key)
            }
        });
    }

    resolve(augmentedIssues || issues);

}

interface IAugmentedJiraIssue extends IJiraIssue {
    history: IHistoryEntry[]
}

interface ISearchOptions {
    max: number;
    chunkSize: number;
    fields: string[];
    history: boolean;
}

interface IAgileJiraApi {
    getSprints(scrumBoardId: number): Promise<ISprint []>;

    getBurndown(scrumBoardId: number, sprintId: number): Promise<ISprintBurndown>;
}

interface IZephyrJiraApi {
    forIssueIds(keys: string[]): Promise<any>;
}

export class JiraTab {
    private chromeTab: Promise<IChromeTab>;

    agile: IAgileJiraApi;
    zephyr: IZephyrJiraApi;

    private baseUrl: string;

    constructor(baseUrl: string) {
        this.baseUrl = baseUrl;
        this.chromeTab = chromeApi.tab.singleton(baseUrl);

        this.agile = {
            getSprints: async (scrumBoardId: number): Promise<ISprint []> => {
                return chromeApi.execute.asContextScript(
                    (await this.chromeTab).id,
                    compose(async (resolve: (data: any) => void, baseUrl: string, scrumBoardId: number) => {
                        const data = await GET(`${baseUrl}/rest/agile/1.0/board/${scrumBoardId}/sprint`);
                        resolve(JSON.parse(data).values)
                    }, GET), baseUrl, scrumBoardId);
            },
            getBurndown: async (scrumBoardId: number, sprintId: number): Promise<ISprintBurndown> => {
                return chromeApi.execute.asContextScript(
                    (await this.chromeTab).id,
                    compose(async (resolve: Function, baseUrl: string, scrumBoardId: number, sprintId: number) => {
                        const data = await GET(`${baseUrl}/rest/greenhopper/1.0/rapid/charts/scopechangeburndownchart?rapidViewId=${scrumBoardId}&sprintId=${sprintId}`);
                        resolve(JSON.parse(data))
                    }, GET), baseUrl, scrumBoardId, sprintId);
            }
        };

        this.zephyr = {
            forIssueIds: async (keys: string[]): Promise<any> => {
                return chromeApi.execute.asContextScript(
                    (await this.chromeTab).id,
                    compose(async (resolve: Function, baseUrl: string, keys: string[]) => {
                        const string = (await Promise.all(keys.map(item => GET(`${baseUrl}/rest/tests/1.0/issue/${item}/tracelinks`))))
                            .map(item => JSON.parse(item));
                        resolve(string);
                    }, GET),
                    baseUrl, keys
                )
            }
        }
    }

    async close() {
        //await chromeApi.tab.close(await this.chromeTab);
    }

    async executeArbitrary<T>(fn: Function, ...args: any[]) {
        return (await chromeApi.execute.asContextScript((await this.chromeTab).id, fn, ...args)) as T;
    }

    async search(jql: string, options?: Partial<ISearchOptions>): Promise<(IJiraIssue | IAugmentedJiraIssue)[]> {
        const defaultOptions: ISearchOptions = {
            max: 300,
            chunkSize: 50,
            fields: ['key', 'status', 'summary', 'assignee', 'issuetype', 'created'],
            history: false
        };

        /* const max = options && 'max' in options ? options.max : defaultOptions.max;
         const chunkSize = options && 'chunkSize' in options ? options.chunkSize : defaultOptions.chunkSize;
         const fields = options && 'fields' in options ? options.fields : defaultOptions.fields;
         const history = options && 'history' in options ? options.history : defaultOptions.history;*/
        const {max, chunkSize, fields, history} = {...defaultOptions, ...options};

        return (await chromeApi.execute.asContextScript((await this.chromeTab).id,
            compose(searchJiraIssues, GET), this.baseUrl, jql, {
                max,
                chunkSize,
                fields,
                history
            }) as (IJiraIssue | IAugmentedJiraIssue)[]);
    }



}